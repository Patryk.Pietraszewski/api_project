import  requests


#Przed uruchomieniem program nalezy wejsc na strone https://developer.brawlstars.com/#/ zarejstrowac sie
#Nastepnie wygenerowac wlasny klucz i podmienic go w zmiennej api_token

class brawl_stars_Api:
    def __init__(self):
        self.api_token='eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiIsImtpZCI6IjI4YTMxOGY3LTAwMDAtYTFlYi03ZmExLTJjNzQzM2M2Y2NhNSJ9.eyJpc3MiOiJzdXBlcmNlbGwiLCJhdWQiOiJzdXBlcmNlbGw6Z2FtZWFwaSIsImp0aSI6IjAyNjg5NmQ3LTczMzEtNDc3Yy04NDE4LTRkMjY3NTNlMGJkNSIsImlhdCI6MTYyMjQ2NTE0Niwic3ViIjoiZGV2ZWxvcGVyL2Y4NjcwNjMzLTViOTMtYzZmMS05Y2QxLTdiZGE0N2ZjNDhlZiIsInNjb3BlcyI6WyJicmF3bHN0YXJzIl0sImxpbWl0cyI6W3sidGllciI6ImRldmVsb3Blci9zaWx2ZXIiLCJ0eXBlIjoidGhyb3R0bGluZyJ9LHsiY2lkcnMiOlsiODAuNTIuMjQyLjEzIl0sInR5cGUiOiJjbGllbnQifV19.uE68Crr6bWrwZDRgx1IaabEvBsYXJJz1DWo0nB-XebG3RYXZk8sSJdTjf90PUPQOBQuY0lWdaAPe0iBjxDi2pg'
        self.headers = {
            'Authorization': "Bearer " + self.api_token, 'Accept': 'application/json'
        }

    def player_tag(self):
        print("WARNING! \nGive shorcut country \ne.g DE-deutsch PL-Poland\n")
        self.country_player=input("Give a player country :")
        self.url=f"https://api.brawlstars.com/v1/rankings/{self.country_player}/players"

        self.result=requests.get(self.url , headers=self.headers)

        return self.result.json()

    def country_clubs(self):
        self.warning=("WARNING! \nGive shorcut country \ne.g DE-deutsch PL-Poland\n")
        print(self.warning,)
        self.tag = input("Give a country tag:")
        self.url = f'https://api.brawlstars.com/v1/rankings/{self.tag}/clubs'



        self.response = requests.get(self.url, headers=self.headers)

        return self.response.json()

    def brawler_info(self):
        self.brawler_id=int(input("Give a number from 0 to 47: "))
        while True:
            if self.brawler_id >=0 and self.brawler_id<=47:
                break
            else:
                print("Bad id!")
                self.brawler_id = int(input("Give a number from 0 to 47: "))
        self.url = f'https://api.brawlstars.com/v1/brawlers/{16000000+self.brawler_id}'
        self.response = requests.get(self.url, headers=self.headers)

        self.info=self.response.json()
        return (self.info)

brawl_fun=brawl_stars_Api()


def menu():
    print("Which option?\n"
          "1.Check player by country\n"
          "2.Check club by country\n"
          "3.Check brawler by id\n"
          "4.Exit")
    choice=int(input("Option: "))
    while True:
        if choice==1:
            print(brawl_fun.player_tag())
            break
        elif choice==2:
            print(brawl_fun.country_clubs())
            break
        elif choice==3:
            print(brawl_fun.brawler_info())
            break
        elif choice==4:
            exit()
        else:
            choice=int(input("Bad number!\nTry again: "))

menu()
